package id.branditya.binarchapter4tugas4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import id.branditya.binarchapter4tugas4.databinding.FragmentLoginBinding
import kotlin.system.exitProcess

class LoginFragment : Fragment() {
    private var _binding : FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, backPressedCallback)
    }

    private val backPressedCallback = object : OnBackPressedCallback(true){
        override fun handleOnBackPressed() {
            exitProcess(-1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreferences = requireContext().getSharedPreferences("id", Context.MODE_PRIVATE)
        btnLoginClicked()
        loginAction()
    }

    private fun btnLoginClicked() {
        binding.apply {
            btnLogin.setOnClickListener {
                val username = etUsername.text.toString()
                val password = etPassword.text.toString()
                when {
                    username.isEmpty() -> {
                        etUsername.error = "Username tidak boleh kosong!"
                    }
                    password.isEmpty() -> {
                        etPassword.error = "Password tidak boleh kosong!"
                    }
                    else -> {
                        val editor = sharedPreferences.edit()
                        editor.putString("username", username)
                        editor.putString("password", password)
                        editor.apply()
                        loginAction()
                    }
                }
            }
        }
    }

    private fun loginAction() {
        val username = sharedPreferences.getString("username", null)
        val password = sharedPreferences.getString("password", null)
        if (username == "DWP" && password == "123"){
            editTextClear()
            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
            createToast("Login Berhasil").show()
        } else if(username != null && password != null) {
            createToast("Username atau Password yang dimasukkan Salah.").show()
        }
    }

    private fun editTextClear(){
        binding.etUsername.text.clear()
        binding.etPassword.text.clear()
    }

    private fun createToast (message : String) : Toast {
        return Toast.makeText(requireContext(),message,Toast.LENGTH_SHORT)
    }
}